﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using TestConsole_Taller3.Entities;

namespace TestConsole_Taller3.Application {
    class MarketEmulator {

        const string HEADER = "Market Emulator 0.5 - Created by Darthoul";
        const string FOOTER = "↑↓:Move ENTER:Select ESC:Exit";

        static public MarketEmulator emulatorInstance;
        public readonly int id;
        public List<Screen> screens;
        private Screen currentScreen;

        static public void Run () {
            emulatorInstance = new MarketEmulator (emulatorInstance == null ? 0 : emulatorInstance.Stop () + 1);
            emulatorInstance.RunEmulation ();
        }

        private void RunEmulation () {
            MarketManager.Initialize ();
            WindowManager.Start (42, 18, HEADER, FOOTER);
            currentScreen = screens[0];
            ConsoleKeyInfo input;
            do {
                WindowManager.WriteScreen (currentScreen);
                input = Console.ReadKey (true);
                switch (input.Key) {
                    case ConsoleKey.UpArrow:
                        currentScreen.Move (-1);
                        break;
                    case ConsoleKey.DownArrow:
                        currentScreen.Move (1);
                        break;
                    case ConsoleKey.Enter:
                        if (currentScreen.options.Length > 0) SwitchCurrentScreen ();
                        break;
                }
            } while (input != null && input.Key != ConsoleKey.Escape);
        }
        private int Stop () {
            return emulatorInstance.id;
        }

        private MarketEmulator (int id) {
            this.id = id;
            screens = new List<Screen> ();
            GenerateScreens ();
        }

        private void GenerateScreens () {
            Screen screen = new Screen ("Would you like a cart or basket?", 2, "Welcome to our Market!");

            //----HAD-TO-
            screen.options[0] = "Cart";
            screen.options[1] = "Basket";
            //-----------

            screens.Add (screen);
            screen = new Screen ("", 0, "Thank you, but the store is closed!\n Btw you chose a:\n ");
            screens.Add (screen);
        }
        private void SwitchCurrentScreen () {
            int selection;
            int currentScreenIndex = screens.BinarySearch (currentScreen.Reset (out selection));

            //----HAD-TO-
            switch (selection) {
                case 0:
                    MarketManager.managerInstance.GetCart ();
                    break;
                case 1:
                    MarketManager.managerInstance.GetBasket ();
                    break;
            }
            screens[1].message += MarketManager.managerInstance.GetData ();
            //-----------

            int targetIndex = 1;
            currentScreen = screens[targetIndex];
        }
    }
}
