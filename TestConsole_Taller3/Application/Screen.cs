﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace TestConsole_Taller3.Application {
    class Screen {

        const string SELECTION = "SELECTION: ";

        public string message;
        public string question;
        public string[] options;
        private int currentOption;

        public string selectionOutput { get { return SELECTION + (currentOption + 1); } }

        public Screen (string question, int optionCount, string message = "") {
            this.question = question;
            this.message = message;
            options = new string[optionCount];
        }

        public int Move (int movement) {
            int targetMovement = currentOption + movement;
            if (targetMovement >= 0 && targetMovement < options.Length) {
                currentOption = targetMovement;
            }
            return currentOption;
        }

        public string GetScreenOutput () {
            string output = string.Empty;

            if (!string.IsNullOrEmpty (message)) {
                output += message + WindowManager.LINE_JUMP;
            }
            output += question + WindowManager.LINE_JUMP;

            for (int i = 0; i < options.Length; i++) {
                output += (i + 1) + "- " + options[i] + WindowManager.LINE_JUMP;
            }

            return output;
        }
        public Screen Reset (out int selectIndex) {
            selectIndex = currentOption;
            currentOption = 0;
            return this;
        }
    }
}
