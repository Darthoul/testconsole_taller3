﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace TestConsole_Taller3.Application {
    static class WindowManager {

        const int MARGIN = 1;
        const string BLANK = " ";
        public const int WRITE_INDEX = 2;
        public const string LINE_JUMP = "\n ";

        private static int currentWidth, currentHeight;
        private static string title, blankFill, endLine;

        public static void Start (int width, int height, string targetTitle, string targetEndline) {
            SetWindowSize (width, height);
            RefreshSize ();
            title = (targetTitle.Length > currentWidth) ? targetTitle.Remove (currentWidth) : targetTitle;
            DrawTitle ();
            endLine = (targetEndline.Length > currentWidth) ? targetEndline.Remove (currentWidth) : targetEndline;
            endLine += new string (BLANK[0], currentWidth - endLine.Length);
            Console.CursorVisible = false;
        }

        public static void WriteScreen (Screen screen) {
            ResetWriteCursor ();
            Console.Write (blankFill);
            SetColors (ConsoleColor.Gray, ConsoleColor.DarkBlue);
            Console.Write (endLine);
            Console.ResetColor ();
            ResetWriteCursor ();

            Console.WriteLine (screen.GetScreenOutput ());
            Console.SetCursorPosition (MARGIN, currentHeight + MARGIN - WRITE_INDEX);
            Console.WriteLine (screen.selectionOutput);
            ResetWriteCursor ();
        }

        private static void SetWindowSize (int width, int height) {
            currentWidth = width;
            currentHeight = height;
            blankFill = string.Empty;
            for (int j = 0; j < (currentHeight - WRITE_INDEX); j++) {
                if (j != currentHeight - WRITE_INDEX - 1) {
                    //blankFill += j.ToString ("00");
                    for (int i = 0; i < currentWidth; i++) {
                        blankFill += BLANK;
                    }
                    if (j == currentHeight - WRITE_INDEX - 1) { break; }
                    blankFill += "\n ";
                }
            }
        }
        private static void RefreshSize () {
            Console.SetWindowSize (currentWidth + (MARGIN * 2), currentHeight + (MARGIN * 2));
            Console.SetBufferSize (currentWidth + (MARGIN * 2), currentHeight + (MARGIN * 2));
        }
        private static void SetColors (ConsoleColor foreground, ConsoleColor background = ConsoleColor.Black) {
            Console.BackgroundColor = background;
            Console.ForegroundColor = foreground;
        }
        private static void DrawTitle () {
            Console.SetCursorPosition (MARGIN, MARGIN);
            SetColors (ConsoleColor.Gray, ConsoleColor.Blue);
            Console.Write (title);
            string fill = string.Empty;
            for (int i = 0; i < currentWidth - title.Length; i++) {
                fill += BLANK;
            }
            Console.WriteLine (fill);
            Console.ResetColor ();
        }
        private static void ResetWriteCursor () {
            Console.SetCursorPosition (0 + MARGIN, WRITE_INDEX + MARGIN);
        }
    }
}
