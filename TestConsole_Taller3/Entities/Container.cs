﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace TestConsole_Taller3.Entities {
    class Container {

        public readonly string name;
        protected List<Product> products;
        public float capacity;

        public Product Search (string productName) {
            return products.Find (product => product.name == productName);
        }
        public void Discard (Product product) {
            products.Remove (product);
        }

        public void AddProduct (Product product) {
            products.Add (product);
        }

        public Container (string name, float capacity) {
            this.name = name;
            this.capacity = capacity;

            products = new List<Product> ();
        }
    }
}
