﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace TestConsole_Taller3.Entities {
    class MarketManager {

        public static MarketManager managerInstance;
        private User currentUser;
        public List<StorageCase> marketContainers;

        public static void Initialize () {
            ProductRelationArchive.InitializeArchive ();
            managerInstance = new MarketManager ();
            managerInstance.currentUser = new User ("Pablo Escobar", 5000);
            managerInstance.marketContainers = new List<StorageCase> ();
            managerInstance.marketContainers.Add (new StorageCase ("Stand1", 9000));
            managerInstance.marketContainers[0].AddProduct (new Artifact ("TV", ProductType.ELECTRONIC, "Toshina"));
        }

        public void GetBasket () {
            currentUser.container = new PortableCase ("Basket", 120);
        }
        public void GetCart () {
            currentUser.container = new PortableCase ("Cart", 450);
        }

        public string GetData () {
            return "User named: " + currentUser.name + " carrying a " + currentUser.container.name;
        }
    }
}
