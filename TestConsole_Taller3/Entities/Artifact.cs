﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace TestConsole_Taller3.Entities {
    class Artifact : Product {

        public string brand;

        public Pack MakePack (Artifact other) {
            return new Pack (name + "/" + other.name, ProductType.REGULAR_PACK);
        }

        public Artifact (string name, ProductType productType, string brand) : base (name, productType) {
            this.brand = brand;
        }
    }
}
