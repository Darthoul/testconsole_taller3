﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace TestConsole_Taller3.Entities {
    class StorageCase : Container {

        public string aisle;

        public StorageCase (string name, float capacity) : base (name, capacity) {

        }

        public void Fill (Product[] products) {
            foreach (Product product in products) {
                this.products.Add (product);
            }
        }
    }
}
