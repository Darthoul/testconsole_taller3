﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace TestConsole_Taller3.Entities {
    class ProductRelationArchive {

        public static ProductRelationArchive archiveInstance;
        public Dictionary<int, float> SKUPricePair;

        public static void InitializeArchive () {
            archiveInstance = new ProductRelationArchive ();
            archiveInstance.SKUPricePair = new Dictionary<int, float> ();
            archiveInstance.SKUPricePair.Add (10, 29);
            archiveInstance.SKUPricePair.Add (11, 15);
            archiveInstance.SKUPricePair.Add (20, 8);
            archiveInstance.SKUPricePair.Add (21, 3);
            archiveInstance.SKUPricePair.Add (30, 40);
            archiveInstance.SKUPricePair.Add (31, 33);
        }

        public float AssignPrice (Product product) {
            return SKUPricePair[(int)product.productType];
        }
    }

    public enum ProductType {
        ELECTRONIC = 10,
        UTENSIL = 11,
        MEAT = 20,
        VEGETABLE = 21,
        REGULAR_PACK = 30,
        MIXED_PACK = 31
    }
}
