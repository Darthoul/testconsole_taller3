﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace TestConsole_Taller3.Entities {
    class Product {

        public readonly string name;
        public ProductType productType { get; protected set; }
        protected float price;
        public float volume;

        public void AddToContainer (Container container) {
            container.AddProduct (this);
        }

        protected Product (string name, ProductType productType) {
            this.name = name;
            this.productType = productType;
            price = ProductRelationArchive.archiveInstance.AssignPrice (this);
        }
    }
}
