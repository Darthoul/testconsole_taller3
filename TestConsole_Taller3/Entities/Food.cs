﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace TestConsole_Taller3.Entities {
    class Food : Product {

        public float weight;
        public int quality;

        public void Rot () {
            quality--;
        }

        public Food (string name, ProductType productType) : base (name, productType) {

        }
    }
}
