﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace TestConsole_Taller3.Entities {
    class PortableCase : Container {

        public PortableCase (string name, float capacity) : base (name, capacity) {

        }

        public void Assign (User user) {
            user.container = this;
        }
    }
}
