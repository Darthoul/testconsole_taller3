﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace TestConsole_Taller3.Entities {
    class User {
        public readonly string name;
        public float moneyCount;
        public PortableCase container;

        public User (string name, int moneyCount) {
            this.name = name;
            this.moneyCount = moneyCount;
        }

        public void BuyProducts () {

        }
    }
}
