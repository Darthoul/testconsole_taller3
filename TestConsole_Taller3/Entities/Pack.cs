﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace TestConsole_Taller3.Entities {
    class Pack : Product {

        public string packId;
        public Product[] products;

        public Pack (string name, ProductType productType) : base (name, productType) {

        }
    }
}
